<?php

namespace Wxl\Captcha;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Factory;
use Intervention\Image\ImageManager;

/**
 * Class CaptchaServiceProvider
 */
class CaptchaServiceProvider extends ServiceProvider
{
	/**
	 * Boot the service provider.
	 *
	 * @return void
	 */
	public function boot(): void
	{
		// Publish configuration files
		$this->publishes([
			__DIR__ . '/../config/captcha.php' => config_path('captcha.php')
		], 'config');


		/* @var Factory $validator */
		$validator = $this->app['validator'];
		// Validator extensions
		$validator->extend('captcha', function ($attribute, $value, $parameters) {
			return captcha_check($value, $parameters[0], $parameters[1] ?? 'default');
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register(): void
	{
		// Merge configs
		$this->mergeConfigFrom(
			__DIR__ . '/../config/captcha.php',
			'captcha'
		);

		// Bind captcha
		$this->app->bind('captcha', function ($app) {
			return new Captcha(
				$app[Filesystem::class],
				$app[Repository::class],
				$app[ImageManager::class],
				$app[BcryptHasher::class]
			);
		});
	}
}
