<?php

namespace Wxl\Captcha;

use Exception;
use Illuminate\Routing\Controller;

/**
 * Class CaptchaController
 * @package Mews\Captcha
 */
class CaptchaController extends Controller
{
	/**
	 * get CAPTCHA
	 *
	 * @param Captcha $captcha
	 * @param string $config
	 * @return array
	 * @throws Exception
	 */
	public function getCaptcha(Captcha $captcha, string $config = 'default'): array
	{
		return $captcha->create($config);
	}
}
